<?php

use PHPUnit\Framework\TestCase;
use Stamphpede\Config\ConfigParser;
use Symfony\Component\Yaml\Exception\ParseException;
use Dotenv\Dotenv;

/**
 * @covers \Stamphpede\Config\ConfigParser
 */
class ConfigParserTest extends TestCase
{

    // Sad Path
    public function testInvalidConfigFile(): void
    {
        $this->expectException(ParseException::class);

        $configFile = __DIR__ . '/iDoNotExist.yaml';

        $sut = new ConfigParser();
        $sut->parseFile($configFile);
    }

    public function testParsedInvalidEnvVar(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Config referenced missing environment variable: I_DO_NOT_EXIST');

        $dotenv = Dotenv::createImmutable(__DIR__ . '/fixtures/', 'parameters.env');
        $dotenv->load();

        $configFile = __DIR__ . '/fixtures/parametersConfigFileWithBadEnv.yaml';

        $sut = new ConfigParser();
        $sut->parseFile($configFile);
    }

    // Happy Path
    public function testConfigParsed(): void
    {
        $configFile = __DIR__ . '/fixtures/parametersConfigFile.yaml';
        $sut = new ConfigParser();
        $config = $sut->parseFile($configFile);

        $this->assertTrue($config->has('baseUrl'));
        $this->assertEquals($config->get('baseUrl'), 'http://localhost');
    }

    public function testConfigParsedWithEnvVar(): void
    {
        $dotenv = Dotenv::createImmutable(__DIR__ . '/fixtures/', 'parameters.env');
        $dotenv->load();

        $configFile = __DIR__ . '/fixtures/parametersConfigFileWithEnv.yaml';
        $sut = new ConfigParser();
        $config = $sut->parseFile($configFile);

        $this->assertTrue($config->has('baseUrl'));
        $this->assertEquals($config->get('baseUrl'), 'http://url-from-env-var.com');

        $this->assertEquals($config->get('adapters')['aws_base_url'], 'http://url-from-env-var.com');
        $this->assertEquals($config->get('adapters')['aws_file'], 'coolfile.zip');
    }

}


