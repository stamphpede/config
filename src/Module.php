<?php

namespace Stamphpede\Config;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class Module
{
    public function getServices(): array
    {
        return [
            ConfigParser::class => [],
            ConfigLoader::class => [
                'args' => [
                    ConfigParser::class,
                ]
            ],
        ];
    }
}
