<?php

namespace Stamphpede\Config;

use Dotenv\Dotenv;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\Yaml\Tag\TaggedValue;
use Symfony\Component\Yaml\Yaml;

class ConfigParser
{
    public function parseFile(string $file): ParameterBag
    {
        $data = (array) Yaml::parseFile($file, Yaml::PARSE_CUSTOM_TAGS);

        foreach ($data as $key => $val) {
            $data[$key] = $this->processVal($val);
        }

        $bag = new ParameterBag($data);

        return $bag;
    }

    /**
     * @param mixed $val
     * @return mixed
     */
    private function processVal($val)
    {
        if (is_array($val)) {

            foreach($val as $arrayKey => $arrayVal) {
                $val[$arrayKey] = $this->processVal($arrayVal);
            }

            return $val;
        }

        if (!($val instanceof TaggedValue)) {
            return $val;
        }

        $envVarName = $val->getValue();

        if ($val->getTag() == 'env' && !isset($_ENV[$envVarName])) {
            throw new \InvalidArgumentException('Config referenced missing environment variable: ' . $envVarName);
        }
        $val = $_ENV[$envVarName];

        return $val;
    }

}
