<?php

namespace Stamphpede\Config;

use Stamphpede\Config\ConfigParser;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;

class ConfigLoader
{
    private ConfigParser $configParser;

    public function __construct(ConfigParser $configParser)
    {
        $this->configParser = $configParser;
    }

    public function loadConfig(array $configPathStack, string $configFilename): ParameterBag
    {
        $configFile = null;

        foreach ($configPathStack as $path) {
            $possibleConfigPath = $this->makePath($path, $configFilename);
            if (file_exists($possibleConfigPath)) {
                $configFile = $possibleConfigPath;
                break;
            }
        }

        if ($configFile === null) {
            throw new \RuntimeException('Unable to find config file');
        }

        return $this->configParser->parseFile($configFile);
    }

    private function makePath(string $dir, string $file): string
    {
        return rtrim($dir, '/') . '/' . ltrim($file, '/');
    }
}
